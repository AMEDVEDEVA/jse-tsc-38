package ru.tsc.golovina.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.command.AbstractCommand;
import ru.tsc.golovina.tm.endpoint.Session;

public class LogoffCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getCommand() {
        return "logoff";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "User logoff from system";
    }

    @Override
    public void execute() {
        @NotNull final Session currentSession = serviceLocator.getSession();
        serviceLocator.getSessionEndpoint().closeSession(currentSession);
    }

}
