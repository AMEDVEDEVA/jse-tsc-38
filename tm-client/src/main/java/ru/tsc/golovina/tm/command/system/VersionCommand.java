package ru.tsc.golovina.tm.command.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.command.AbstractCommand;

public class VersionCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getCommand() {
        return "version";
    }

    @Nullable
    @Override
    public String getArgument() {
        return "-v";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Display program version";
    }

    @Override
    public void execute() {
        System.out.println(Manifests.read("version"));
    }

}
