package ru.tsc.golovina.tm.exception.empty;

import ru.tsc.golovina.tm.exception.AbstractException;

public class EmptyUserListException extends AbstractException {

    public EmptyUserListException() {
        super("Error. User list is empty.");
    }

}
