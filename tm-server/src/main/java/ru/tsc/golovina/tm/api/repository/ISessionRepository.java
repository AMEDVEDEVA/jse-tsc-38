package ru.tsc.golovina.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import ru.tsc.golovina.tm.api.IRepository;
import ru.tsc.golovina.tm.model.Session;

import java.sql.SQLException;
import java.util.List;

public interface ISessionRepository {

    @Insert("INSERT INTO sessions" +
            " (id, date, signature, user_id)" +
            " VALUES (#{session.id}, #{session.date}, #{session.signature}, #{session.userId});")
    void open(@NotNull @Param("session") final Session session);

    @Update("DELETE FROM sessions WHERE id = #{session.id};")
    int close(@NotNull @Param("session") final Session session);

    @Select("SELECT * FROM sessions WHERE id = #{id};")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    Session contains(@NotNull @Param("id") final String id) throws SQLException;

    @Delete("DELETE FROM sessions WHERE id = #{session.id}")
    void remove(@NotNull @Param("session") Session session);

    @Delete("DELETE FROM sessions")
    void clear();

    @NotNull
    @Select("SELECT * FROM sessions")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    List<Session> findAll();

    @NotNull
    @Select("SELECT * FROM sessions WHERE id = #{id}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    Session findById(@NotNull @Param("id") String id);

    @NotNull
    @Select("SELECT * FROM sessions LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    Session findByIndex(@Param("index") int index);

    @Delete("DELETE FROM sessions WHERE id = #{id}")
    void removeById(@NotNull @Param("id") String id);

}
