package ru.tsc.golovina.tm.component;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.api.IPropertyService;
import ru.tsc.golovina.tm.api.repository.IProjectRepository;
import ru.tsc.golovina.tm.api.repository.ISessionRepository;
import ru.tsc.golovina.tm.api.repository.ITaskRepository;
import ru.tsc.golovina.tm.api.repository.IUserRepository;
import ru.tsc.golovina.tm.api.service.*;
import ru.tsc.golovina.tm.endpoint.ProjectEndpoint;
import ru.tsc.golovina.tm.endpoint.TaskEndpoint;
import ru.tsc.golovina.tm.enumerated.Role;
import ru.tsc.golovina.tm.model.User;
import ru.tsc.golovina.tm.repository.ProjectRepository;
import ru.tsc.golovina.tm.repository.SessionRepository;
import ru.tsc.golovina.tm.repository.TaskRepository;
import ru.tsc.golovina.tm.repository.UserRepository;
import ru.tsc.golovina.tm.service.*;
import ru.tsc.golovina.tm.util.HashUtil;

import javax.xml.ws.Endpoint;

@Getter
public class Bootstrap implements IServiceLocator {

    @NotNull
    public final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService);

    @NotNull
    private final IUserService userService = new UserService(connectionService, propertyService);

    @NotNull
    private final ISessionService sessionService = new SessionService(connectionService, userService, propertyService);

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(projectService, sessionService);

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpoint(taskService, sessionService);

    private void initEndpoint() {
        registry(projectEndpoint);
        registry(taskEndpoint);
    }

    public void start(@Nullable final String... args) {
        initUsers();
        initEndpoint();
    }

    public void initUsers() {
        final User user = new User("user", HashUtil.salt(propertyService, "user"));
        user.setEmail("user@email.ru");
        userService.add(user);
        final User admin = new User("admin", HashUtil.salt(propertyService, "admin"));
        admin.setRole(Role.ADMIN);
        userService.add(admin);
    }

    private void registry(final @NotNull Object endpoint) {
        final String host = propertyService.getServerHost();
        final String port = propertyService.getServerPort();
        final String name = endpoint.getClass().getSimpleName();
        final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

}
