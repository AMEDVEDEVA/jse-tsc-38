package ru.tsc.golovina.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.tsc.golovina.tm.api.endpoint.ISessionEndpoint;
import ru.tsc.golovina.tm.api.service.ISessionService;
import ru.tsc.golovina.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@NoArgsConstructor
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    private ISessionService sessionService;

    public SessionEndpoint(final ISessionService sessionService) {
        this.sessionService = sessionService;
    }

    @WebMethod
    public @NotNull Session openSession(
            @NotNull @WebParam(name = "login") final String login,
            @NotNull @WebParam(name = "password") final String password
    ) {
        return sessionService.open(login, password);
    }

    @WebMethod
    public void closeSession(@NotNull @WebParam(name = "session") final Session session) {
        sessionService.close(session);
    }

}
