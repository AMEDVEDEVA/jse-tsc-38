package ru.tsc.golovina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.api.repository.IProjectRepository;
import ru.tsc.golovina.tm.api.service.IConnectionService;
import ru.tsc.golovina.tm.api.service.IProjectService;
import ru.tsc.golovina.tm.exception.empty.*;
import ru.tsc.golovina.tm.exception.system.DatabaseException;
import ru.tsc.golovina.tm.model.Project;
import ru.tsc.golovina.tm.repository.ProjectRepository;

import java.sql.Connection;
import java.sql.SQLException;

public final class ProjectService extends AbstractOwnerService<Project> implements IProjectService {

    public ProjectService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    public IProjectRepository getRepository(@NotNull final Connection connection) {
        return new ProjectRepository(connection);
    }



    @Override
    public Project create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IProjectRepository projectRepository = getRepository(connection);
            projectRepository.add(project);
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
        return project;
    }

}