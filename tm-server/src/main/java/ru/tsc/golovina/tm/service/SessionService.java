package ru.tsc.golovina.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.api.IPropertyService;
import ru.tsc.golovina.tm.api.IRepository;
import ru.tsc.golovina.tm.api.repository.ISessionRepository;
import ru.tsc.golovina.tm.api.service.IConnectionService;
import ru.tsc.golovina.tm.api.service.ISessionService;
import ru.tsc.golovina.tm.api.service.IUserService;
import ru.tsc.golovina.tm.enumerated.Role;
import ru.tsc.golovina.tm.exception.system.AccessDeniedException;
import ru.tsc.golovina.tm.exception.system.DatabaseException;
import ru.tsc.golovina.tm.model.Session;
import ru.tsc.golovina.tm.model.User;
import ru.tsc.golovina.tm.repository.SessionRepository;
import ru.tsc.golovina.tm.util.HashUtil;

import java.sql.Connection;
import java.sql.SQLException;

import static org.reflections.util.Utils.isEmpty;

public class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    @Override
    protected ISessionRepository getRepository(@NotNull final Connection connection) {
        return new SessionRepository(connection);
    }

    public SessionService(@NotNull IConnectionService connectionService, @NotNull final IUserService userService,
                          @NotNull IPropertyService propertyService) {
        super(connectionService);
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @Override
    @NotNull
    public Session open(@NotNull final String login, @NotNull final String password) {
        final boolean check = checkDataAccess(login, password);
        @NotNull final User user = userService.findUserByLogin(login);
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final ISessionRepository sessionRepository = getRepository(connection);
            sessionRepository.add(session);
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
        return session;
    }

    @SneakyThrows
    @Override
    @NotNull
    public Session sign(@NotNull final Session session) {
        session.setSignature(null);
        @NotNull final String salt = propertyService.getSessionSecret();
        final int cycle = propertyService.getSessionIteration();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(session);
        @NotNull final String signature = HashUtil.salt(salt, cycle, json);
        session.setSignature(signature);
        return session;
    }

    @Override
    public boolean close(@NotNull Session session) {
        validate(session);
        remove(session);
        return false;
    }

    @Override
    public boolean checkDataAccess(@NotNull String login, @NotNull String password) {
        @NotNull final User user = userService.findUserByLogin(login);
        @NotNull final String passwordHash = HashUtil.salt(propertyService, password);
        return passwordHash.equals(user.getPassword());
    }

    @Override
    public void validate(@NotNull final Session session) {
        if (isEmpty(session.getSignature())) throw new AccessDeniedException();
        if (isEmpty(session.getUserId())) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        @NotNull final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @NotNull final Session sessionSign = sign(temp);
        @Nullable final String signatureTarget = sessionSign.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final ISessionRepository sessionRepository = getRepository(connection);
            if (!sessionRepository.contains(session.getId())) throw new AccessDeniedException();
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @Override
    public void validate(@NotNull Session session, @NotNull Role role) {
        validate(session);
        @NotNull final String userId = session.getUserId();
        @NotNull final User user = userService.findById(userId);
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }

}
