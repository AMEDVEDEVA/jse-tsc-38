package ru.tsc.golovina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.api.IPropertyService;
import ru.tsc.golovina.tm.api.repository.IUserRepository;
import ru.tsc.golovina.tm.api.service.IConnectionService;
import ru.tsc.golovina.tm.api.service.IUserService;
import ru.tsc.golovina.tm.enumerated.Role;
import ru.tsc.golovina.tm.exception.empty.*;
import ru.tsc.golovina.tm.exception.entity.UserEmailExistsException;
import ru.tsc.golovina.tm.exception.entity.UserLoginExistsException;
import ru.tsc.golovina.tm.exception.entity.UserNotFoundException;
import ru.tsc.golovina.tm.exception.system.DatabaseException;
import ru.tsc.golovina.tm.model.User;
import ru.tsc.golovina.tm.repository.UserRepository;
import ru.tsc.golovina.tm.util.HashUtil;

import java.sql.Connection;
import java.sql.SQLException;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    public UserService(@NotNull IConnectionService connectionService, @NotNull IPropertyService propertyService) {
        super(connectionService);
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public IUserRepository getRepository(@NotNull final Connection connection) {
        return new UserRepository(connection);
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (isLoginExists(login)) throw new UserLoginExistsException(login);
        @NotNull final User user = new User(login, HashUtil.salt(propertyService, password));
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            repository.add(user);
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
        return user;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (isEmailExists(email)) throw new UserEmailExistsException(email);
        @NotNull final User user = create(login, password);
        user.setEmail(email);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            repository.add(user);
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
        return user;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        @NotNull final User user = create(login, password);
        user.setRole(role);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            repository.add(user);
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
        return user;
    }

    @Override
    public void setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final User user = findById(id);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            user.setPassword(HashUtil.salt(propertyService, password));
            connection.commit();
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @Override
    public void setRole(@Nullable final String id, @Nullable final Role role) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (role == null) throw new EmptyRoleException();
        @NotNull final User user = findById(id);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            user.setRole(role);
            connection.commit();
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @NotNull
    @Override
    public User findUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user;
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            user = repository.findUserByLogin(login);
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @NotNull
    @Override
    public User findUserByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @Nullable final User user;
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            user = repository.findUserByEmail(email);
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @Override
    public void removeUserById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            repository.removeById(id);
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @NotNull
    @Override
    public User removeUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user;
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            user = repository.findUserByLogin(login);
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @Override
    public boolean isLoginExists(@Nullable final String login) {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            return repository.findUserByLogin(login) != null;
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @Override
    public boolean isEmailExists(@NotNull final String email) {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            return repository.findUserByEmail(email) != null;
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @Override
    public void updateUserById(
            @Nullable final String id, @Nullable final String lastName, @Nullable final String firstName,
            @Nullable final String middleName, @Nullable final String email
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (lastName == null || lastName.isEmpty()) throw new EmptyLastNameException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyFirstNameException();
        if (middleName == null || middleName.isEmpty()) throw new EmptyMiddleNameException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (isEmailExists(email)) throw new UserEmailExistsException(email);
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            @Nullable final User user = repository.findById(id);
            if (user == null) throw new UserNotFoundException();
            user.setLastName(lastName);
            user.setFirstName(firstName);
            user.setMiddleName(middleName);
            user.setEmail(email);
            connection.commit();
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @Override
    public void updateUserByLogin(
            @Nullable final String login, @Nullable final String lastName, @Nullable final String firstName,
            @Nullable final String middleName, @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (isLoginExists(email)) throw new UserLoginExistsException(login);
        if (lastName == null || lastName.isEmpty()) throw new EmptyLastNameException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyFirstNameException();
        if (middleName == null || middleName.isEmpty()) throw new EmptyMiddleNameException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            @Nullable final User user = repository.findUserByLogin(login);
            if (user == null) throw new UserNotFoundException();
            user.setLastName(lastName);
            user.setFirstName(firstName);
            user.setMiddleName(middleName);
            user.setEmail(email);
            connection.commit();
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            @NotNull final User user = findUserByLogin(login);
            user.setLocked(true);
            connection.commit();
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            @NotNull final User user = findUserByLogin(login);
            user.setLocked(false);
            connection.commit();
        } catch (SQLException exception) {
            throw new DatabaseException();
        }
    }

}